if vim.g.neovide then
  vim.o.guifont = "Source Code Pro:h14"
  vim.g.neovide_floating_blur_amount_x = 2.0
  vim.g.neovide_floating_blur_amount_y = 2.0
end
