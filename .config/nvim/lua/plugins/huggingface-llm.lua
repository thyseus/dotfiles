if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

return {
  {
    "huggingface/llm.nvim",
    opts = {
      backend = "openai",
      -- backend = "ollama",
      -- model = "deepseek-r1:7b",
      -- model = "codellama:7b",
      url = "http://localhost:11434",
      
      request_body = {
        -- Modelfile options for the model you use
        options = {
          temperature = 0.2,
          top_p = 0.95,
        }
      }
    }
  }
}
