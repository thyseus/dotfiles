---@type LazySpec
return {

  -- == Examples of Adding Plugins ==
  --
  -- {
  --   "https://github.com/fresh2dev/zellij.vim.git",
  --   lazy = false,
  -- },

  -- "andweeb/presence.nvim",
  -- "sindrets/diffview.nvim",
  -- {
  --   "folke/snacks.nvim",
  --   priority = 1000,
  --   lazy = false,
  --   opts = {
  --     bigfile = { enabled = true },
  --     notifier = { enabled = true },
  --     quickfile = { enabled = true },
  --     statuscolumn = { enabled = true },
  --     words = { enabled = true },
  --   },
  -- },
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    config = function() require("lsp_signature").setup() end,
  },

  -- customize alpha options
  {
    "goolord/alpha-nvim",
    opts = function(_, opts)
      -- customize the dashboard header
      opts.section.header.val = {
        " █████  ███████ ████████ ██████   ██████",
        "██   ██ ██         ██    ██   ██ ██    ██",
        "███████ ███████    ██    ██████  ██    ██",
        "██   ██      ██    ██    ██   ██ ██    ██",
        "██   ██ ███████    ██    ██   ██  ██████",
        " ",
        "    ███    ██ ██    ██ ██ ███    ███",
        "    ████   ██ ██    ██ ██ ████  ████",
        "    ██ ██  ██ ██    ██ ██ ██ ████ ██",
        "    ██  ██ ██  ██  ██  ██ ██  ██  ██",
        "    ██   ████   ████   ██ ██      ██",
      }
      return opts
    end,
  },
  -- You can disable default plugins as follows:
  { "max397574/better-escape.nvim", enabled = true },

  -- You can also easily customize additional setup of plugins that is outside of the plugin's setup call
  {
    "L3MON4D3/LuaSnip",
    config = function(plugin, opts)
      require "astronvim.plugins.configs.luasnip"(plugin, opts) -- include the default astronvim config that calls the setup call
      -- add more custom luasnip configuration such as filetype extend or custom snippets
      local luasnip = require "luasnip"
      luasnip.filetype_extend("javascript", { "javascriptreact" })
    end,
  },

  {
    "windwp/nvim-autopairs",
    config = function(plugin, opts)
      require "astronvim.plugins.configs.nvim-autopairs"(plugin, opts) -- include the default astronvim config that calls the setup call
      -- add more custom autopairs configuration such as custom rules
      local npairs = require "nvim-autopairs"
      local Rule = require "nvim-autopairs.rule"
      local cond = require "nvim-autopairs.conds"
      npairs.add_rules(
        {
          Rule("$", "$", { "tex", "latex" })
            -- don't add a pair if the next character is %
            :with_pair(cond.not_after_regex "%%")
            -- don't add a pair if  the previous character is xxx
            :with_pair(
              cond.not_before_regex("xxx", 3)
            )
            -- don't move right when repeat character
            :with_move(cond.none())
            -- don't delete if the next character is xx
            :with_del(cond.not_after_regex "xx")
            -- disable adding a newline when you press <cr>
            :with_cr(cond.none()),
        },
        -- disable for .vim files, but it work for another filetypes
        Rule("a", "a", "-vim")
      )
    end,
  },
  {
    "nvim-neotest/neotest",
    dependencies = {
      "nvim-neotest/nvim-nio",
      "nvim-lua/plenary.nvim",
      "antoinemadec/FixCursorHold.nvim",
      "nvim-treesitter/nvim-treesitter",
      -- "olimorris/neotest-phpunit",
      "V13Axel/neotest-pest",
      "rouge8/neotest-rust",
    },
    config = function()
      require("neotest").setup {
        adapters = {
          require "neotest-rust",
          --  require "neotest-phpunit",
          require "neotest-pest" {
             sail_enabled = true,
          },
        },
      }
    end,
  },
  -- {
  --   "ta-tikoma/php.easy.nvim",
  --   config = true,
  --   keys = {
  --     { "-b", "<CMD>PHPEasyDocBlock<CR>" },
  --     { "-r", "<CMD>PHPEasyReplica<CR>" },
  --     { "-c", "<CMD>PHPEasyCopy<CR>" },
  --     { "-d", "<CMD>PHPEasyDelete<CR>" },
  --     { "-ii", "<CMD>PHPEasyInitInterface<CR>" },
  --     { "-ic", "<CMD>PHPEasyInitClass<CR>" },
  --     { "-iac", "<CMD>PHPEasyInitAbstractClass<CR>" },
  --     { "-it", "<CMD>PHPEasyInitTrait<CR>" },
  --     { "-ie", "<CMD>PHPEasyInitEnum<CR>" },
  --     { "-c", "<CMD>PHPEasyAppendConstant<CR>", mode = { "n", "v" } },
  --     { "-p", "<CMD>PHPEasyAppendProperty<CR>", mode = { "n", "v" } },
  --     { "-m", "<CMD>PHPEasyAppendMethod<CR>", mode = { "n", "v" } },
  --     { "-_", "<CMD>PHPEasyAppendConstruct<CR>" },
  --     { "-a", "<CMD>PHPEasyAppendArgument<CR>" },
  --   },
  -- },
  -- {
  --   "ricardoramirezr/.nvim",
  --   ft = {"blade", "php" }
  -- },
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    config = function() require("lsp_signature").setup() end,
  },
  { "rafamadriz/friendly-snippets" },
  {
    "L3MON4D3/LuaSnip",
    dependencies = { "rafamadriz/friendly-snippets" },
    config = function(plugin, opts)
      require "astronvim.plugins.configs.luasnip"(plugin, opts) -- include the default astronvim config that calls the setup call
      -- add more custom luasnip configuration such as filetype extend or custom snippets
      local luasnip = require "luasnip"
      luasnip.filetype_extend("javascript", { "javascriptreact" })
      luasnip.filetype_extend("php", { "php", "laravel" })
    end,
  },

  {
    "https://git.sr.ht/~swaits/zellij-nav.nvim",
    lazy = true,
    event = "VeryLazy",
    keys = {
      { "<c-h>", "<cmd>ZellijNavigateLeft<cr>", { silent = true, desc = "navigate left" } },
      { "<c-j>", "<cmd>ZellijNavigateDown<cr>", { silent = true, desc = "navigate down" } },
      { "<c-k>", "<cmd>ZellijNavigateUp<cr>", { silent = true, desc = "navigate up" } },
      { "<c-l>", "<cmd>ZellijNavigateRight<cr>", { silent = true, desc = "navigate right" } },
    },
    opts = {},
  },
}
