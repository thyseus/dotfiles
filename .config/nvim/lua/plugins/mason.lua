-- customize mason plugins
return {
  -- use mason-lspconfig to configure LSP installations
  {
    "williamboman/mason-lspconfig.nvim",
    -- overrides `require("mason-lspconfig").setup(...)`
    opts = {
      -- ensure_installed = { "lua_ls" },
    },
  },
  -- use mason-null-ls to configure Formatters/Linter installation for null-ls sources
  {
    "jay-babu/mason-null-ls.nvim",
    -- overrides `require("mason-null-ls").setup(...)`
    opts = {
      -- ensure_installed = { "prettier", "stylua" },
    },
  },
  {
    "jay-babu/mason-nvim-dap.nvim",
--    overrides `require("mason-nvim-dap").setup(...)`
    opts = {
     ensure_installed = {
        "codelldb",
        "python",
        "php"
      },
      automatic_setup = true,
      handlers = {
        php = function(config)
          require("mason-nvim-dap").default_setup(config)
          local dap = require "dap"
          dap.adapters.php = {
            type = "executable",
            command = os.getenv("HOME") .. "/.local/share/nvim/mason/bin/php-debug-adapter",
          }

          -- dap.adapters.php = {
          --   type = "executable",
          --   command = "node",
          --   args = { os.getenv("HOME") .. "/dev/vscode-php-debug/out/phpDebug.js" }
          -- }

          dap.configurations.php = {
            {
              type = 'php',
              request = "launch",
              port = 9003,
              log = true,
              name = 'Listen for Xdebug',
              pathMappings = {
                ["${workspaceFolder}"] = "${workspaceFolder}",
                ["/var/www/html"] = "${workspaceFolder}"
              }
            },
          }

        end,
      },
    },
  }
}
