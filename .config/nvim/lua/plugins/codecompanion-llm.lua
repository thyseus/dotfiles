if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

return {
  "olimorris/codecompanion.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-treesitter/nvim-treesitter",
  },
  config = function(plugin, opts)
    require("codecompanion").setup {
      strategies = {
        chat = {
          adapter = "openai",
          -- adapter = "ollama",
        },
        inline = {
          adapter = "openai",
          -- adapter = "ollama",
        },
        sources = {
          per_filetype = {
            codecompanion = { "codecompanion" },
          }
        },
      },
      -- adapters = {
      --   ollama = function()
      --     return require("codecompanion.adapters").extend("ollama", {
      --       env = {
      --         url = "http://localhost:11434",
      --         --api_key = "OLLAMA_API_KEY",
      --       },
      --       --              headers = {
      --       --               ["Content-Type"] = "application/json",
      --       --              ["Authorization"] = "Bearer ${api_key}",
      --       --           },
      --       parameters = {
      --         sync = true,
      --       },
      --     })
      --   end,
      -- },
    }
  end,
}
