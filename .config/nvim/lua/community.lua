-- AstroCommunity: import any community modules here
-- We import this file in `lazy_setup.lua` before the `plugins/` folder.
-- This guarantees that the specs are processed before any user plugins.

---@type LazySpec
return {
  "AstroNvim/astrocommunity",

  -- AI:
  -- { import = "astrocommunity.ai.kurama622-llm-nvim" },
  -- { import = "astrocommunity.completion.avante-nvim" },

  { import = "astrocommunity.search.nvim-spectre" },

  { import = "astrocommunity.git.git-blame-nvim" },
  -- { import = "astrocommunity.git.diffview-nvim" },
  -- { import = "astrocommunity.git.mini-diff" },
  -- { import = "astrocommunity.git.neogit" },

  -- { import = "astrocommunity.quickfix.nvim-bqf" },
  -- { import = "astrocommunity.quickfix.quicker-nvim" },

  -- { import = "astrocommunity.diagnostics.error-lens-nvim" },
  -- { import = "astrocommunity.diagnostics.lsp_lines-nvim" },
  { import = "astrocommunity.diagnostics.trouble-nvim" },
  -- { import = "astrocommunity.diagnostics.tiny-inline-diagnostic-nvim" },

  -- { import = "astrocommunity.file-explorer.telescope-file-browser-nvim" },

  -- { import = "astrocommunity.bars-and-lines.bufferline-nvim" },
  -- { import = "astrocommunity.bars-and-lines.scope-nvim" },

  -- { import = "astrocommunity.bars-and-lines.smartcolumn-nvim" },
  -- { import = "astrocommunity.bars-and-lines.vim-illuminate" },
  -- { import = "astrocommunity.bars-and-lines.scope-nvim" },
  -- { import = "astrocommunity.bars-and-lines.lualine-nvim" },
  -- { import = "astrocommunity.bars-and-lines.feline-nvim" },
  -- { import = "astrocommunity.bars-and-lines.dropbar-nvim" },
  { import = "astrocommunity.bars-and-lines.statuscol-nvim" },

  -- { import = "astrocommunity.lsp.lspsaga-nvim" },
  -- { import = "astrocommunity.lsp.nvim-lint" },

  -- { import = "astrocommunity.motion.mini-jump" },
  -- { import = "astrocommunity.motion.vim-matchup" },
  -- { import = "astrocommunity.motion.leap-nvim" },

  -- { import = "astrocommunity.color.nvim-highlight-colors" },

  -- { import = "astrocommunity.editing-support.nvim-treesitter-endwise" },
  -- { import = "astrocommunity.editing-support.rainbow-delimiters-nvim" },
  -- { import = "astrocommunity.editing-support.neogen" },
  -- { import = "astrocommunity.editing-support.nvim-devdocs" },
  -- { import = "astrocommunity.editing-support.nvim-context-vt" },
  -- { import = "astrocommunity.editing-support.todo-comments-nvim" },
  -- { import = "astrocommunity.editing-support.treesj" },
  -- { import = "astrocommunity.editing-support.vim-visual-multi" },
  -- { import = "astrocommunity.editing-support.chatgpt-nvim" },

  -- { import = "astrocommunity.terminal-integration.vim-tpipeline" },
  -- { import = "astrocommunity.terminal-integration.vim-tmux-yank" },

  -- { import = "astrocommunity.completion.cmp-calc" },
  -- { import = "astrocommunity.completion.nvim-cmp-buffer-lines" },
  -- { import = "astrocommunity.completion.cmp-cmdline" },
  -- { import = "astrocommunity.completion.cmp-emoji" },
  -- { import = "astrocommunity.completion.cmp-git" },
  -- { import = "astrocommunity.completion.magazine-nvim" },

  -- { import = "astrocommunity.completion.blink-cmp" },
  -- { import = "astrocommunity.completion.cmp-tmux" },
  -- { import = "astrocommunity.completion.mini-completion" },
  -- { import = "astrocommunity.completion.cmp-spell" },
  -- { import = "astrocommunity.completion.cmp-under-comparator" },

  -- { import = "astrocommunity.syntax.hlargs-nvim" },
  -- { import = "astrocommunity.syntax.vim-cool" },

  -- { import = "astrocommunity.code-runner.vim-slime" },
  -- { import = "astrocommunity.code-runner.mf-runner-nvim" },

  -- { import = "astrocommunity.recipes.neovide" },
  -- { import = "astrocommunity.recipes.astrolsp-no-insert-inlay-hints" },
  -- { import = "astrocommunity.recipes.auto-session-restore" },
  { import = "astrocommunity.recipes.picker-lsp-mappings" },


  { import = "astrocommunity.fuzzy-finder.namu-nvim" },

  -- { import = "astrocommunity.session.vim-workspace" },

  -- { import = "astrocommunity.programming-language-support.csv-vim" },
  -- { import = "astrocommunity.programming-language-support.nvim-jqx" },

  -- { import = "astrocommunity.split-and-window.colorful-winsep-nvim" },
  -- { import = "astrocommunity.split-and-window.neominimap-nvim" },

  -- currently disabled in favor of https://hurl.dev/
  -- { import = "astrocommunity.programming-language-support.rest-nvim" },

  { import = "astrocommunity.test.neotest" },
  -- { import = "astrocommunity.test.vim-test" },

  -- { import = "astrocommunity.media.codesnap-nvim" },

  -- { import = "astrocommunity.remote-development.remote-sshfs-nvim" },

  { import = "astrocommunity.debugging.nvim-dap-repl-highlights" },
  { import = "astrocommunity.debugging.persistent-breakpoints-nvim" },
  { import = "astrocommunity.debugging.telescope-dap-nvim" },

  -- { import = "astrocommunity.workflow.precognition-nvim" },
  -- { import = "astrocommunity.workflow.bad-practices-nvim" },

  { import = "astrocommunity.utility.noice-nvim" },
  -- { import = "astrocommunity.utility.neodim" },
  -- { import = "astrocommunity.utility.hover-nvim" },

  -- { import = "astrocommunity.pack.astro" },
  -- { import = "astrocommunity.pack.ansible" },
  -- { import = "astrocommunity.pack.bash" },
  -- { import = "astrocommunity.pack.blade" },
  -- { import = "astrocommunity.pack.html-css" },
  -- { import = "astrocommunity.pack.docker" },
  { import = "astrocommunity.pack.full-dadbod" },
  -- { import = "astrocommunity.pack.json" },
  -- { import = "astrocommunity.pack.markdown" },
  -- { import = "astrocommunity.pack.php" },
  -- { import = "astrocommunity.pack.laravel" },
  -- { import = "astrocommunity.pack.python" },
  -- { import = "astrocommunity.pack.lua" },
  -- { import = "astrocommunity.pack.rust" },
  -- { import = "astrocommunity.pack.tailwindcss" },
  -- { import = "astrocommunity.pack.typescript" },
  -- { import = "astrocommunity.pack.toml" },
  -- { import = "astrocommunity.pack.yaml" },

  -- { import = "astrocommunity.pack.nvchad-ui" },
}
