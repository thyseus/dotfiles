# This is a collection of my personal dotfiles for public use.

And for myself for quickly setting up a new development workspace in minutes.
I mainly develop in php, ruby, javascript and golang with vim.
My favourite distribution is https://mxlinux.org/ .
Run `make install` to run everything or do `head Makefile` to see possibilities.

### firefox extensions:

| Extension                 | Firefox                                                                |
| ------------------------- | ---------------------------------------------------------------------- |
| Privacy Badger            | https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/       |
| Ghostery                  | https://addons.mozilla.org/de/firefox/addon/ghostery/                  |
| uBlock origin             | https://addons.mozilla.org/de/firefox/addon/ublock-origin/             |
| I dont care about Cookies | https://addons.mozilla.org/de/firefox/addon/i-dont-care-about-cookies/ |

### laravel package collection:

- https://log-viewer.opcodes.io/docs/3.x
- https://github.com/barryvdh/laravel-debugbar
- https://github.com/laracraft-tech/laravel-schema-rules
- https://validationforlaravel.com/
- https://github.com/prasathmani/tinyfilemanager

### optional software:

| Software                 | Url                                                                                                         | Comment                                      |
| ------------------------ | ----------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| glab                     | https://gitlab.com/gitlab-org/cli/                                                                          |                                              |
| Mousam                   | https://github.com/amit9838/mousam                                                                          | Beautiful weather software                   |
| Jetbrains mono .ttf font | https://www.jetbrains.com/lp/mono/#how-to-install                                                           | -                                            |
| modelio                  | https://www.modelio.org/downloads/download-modelio.html                                                     | use debian, not ubuntu for linux mint 20!    |
| gitlab runner            | `curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh \| sudo bash;` | apt install gitlab-runner                    |
| youtube-dl               | https://yt-dl.org/download.html                                                                             | -                                            |
| gomuks                   | https://github.com/tulir/gomuks/releases                                                                    | matrix client (watch for .deb release)       |
| dsq                      | https://github.com/multiprocessio/dsq                                                                       |                                              |
| grex                     | https://github.com/pemistahl/grex/releases/                                                                 | (regular expression generator)               |
| git-delta                | https://github.com/dandavison/delta/releases                                                                | -                                            |
| wtf                      | https://github.com/wtfutil/wtf/releases                                                                     | -                                            |
| layzdocker               | https://github.com/jesseduffield/lazydocker/releases                                                        | -                                            |
| glow                     | https://github.com/charmbracelet/glow/releases                                                              | Markdown in Terminal                         |
| skipper                  | https://www.skipper18.com/en/download                                                                       | graphical ORM design tool, supports Eloquent |
| cyberchef                | https://gchq.github.io/CyberChef/                                                                           |                                              |
| regex101                 | https://regex101.com/                                                                                       |                                              |
| drawio                   | https://github.com/jgraph/drawio-desktop/releases/                                                          |                                              |
| obsidian                 | https://obsidian.md/                                                                                        | A second brain, for you, forever.            |
| heroicgameslauncher      | https://flathub.org/apps/details/com.heroicgameslauncher.hgl                                                |
| gog-backup               | https://github.com/evanpowers/gog-backup                                                                    |
| yq (jq for yaml)         | https://github.com/mikefarah/yq                                                                             |
