<?php

$finder = PhpCsFixer\Finder::create()
	->in(__DIR__)
	->name('*.php')
	->name('*.inc')
	->name('*.phtml');

$config = new PhpCsFixer\Config();

return $config
	->setIndent("\t")
	->setLineEnding("\n")
	->setRules([
		'@PSR2'                         => true,
		'@PhpCsFixer'                   => true,
		'binary_operator_spaces'        => false, // keep left and right area of = sign untouched
		'yoda_style'                    => false,
		'ordered_class_elements'        => false,
		'ordered_imports'               => false,
		'phpdoc_summary'                => false,
		'concat_space'                  => false, // dont touch string concatenation dots ('foo'   .   'bar')
		'increment_style'               => false, // dont touch $a++ or ++$a
		'return_assignment'             => false,

		'multiline_whitespace_before_semicolons' => ['strategy' => 'no_multi_line'],
	])
	->setFinder($finder);
