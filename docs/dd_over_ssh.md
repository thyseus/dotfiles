https://unix.stackexchange.com/questions/132797/how-to-dd-a-remote-disk-using-ssh-on-local-machine-and-save-to-a-local-disk

# from source to target ("push"):

dd if=/dev/sda | gzip -1 - | ssh thyseus@192.168.2.105 dd of=/home/thyseus/target.gz

# to target from source ("pull"):

ssh user@remote "dd if=/dev/sda | gzip -1 -" | dd of=image.gz
