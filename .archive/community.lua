return {
  "AstroNvim/astrocommunity",

  -- sensible defaults:
  { import = "astrocommunity.utility.noice-nvim" },
  { import = "astrocommunity.file-explorer.mini-files" },

  -- packs:
  { import = "astrocommunity.pack.astro" },
  { import = "astrocommunity.pack.ansible" },
  { import = "astrocommunity.pack.bash" },
  { import = "astrocommunity.pack.docker" },
  { import = "astrocommunity.pack.full-dadbod" },
  { import = "astrocommunity.pack.json" },
  { import = "astrocommunity.pack.markdown" },
  { import = "astrocommunity.pack.python" },
  { import = "astrocommunity.pack.toml" },
  { import = "astrocommunity.pack.tailwindcss" },
  { import = "astrocommunity.pack.typescript" },
  { import = "astrocommunity.pack.toml" },
  { import = "astrocommunity.pack.yaml" },
  -- better install *only* intelephense manually; TSInstall php, LspInstall php:
  -- { import = "astrocommunity.pack.php" },

  { import = "astrocommunity.project.nvim-spectre" },

  -- Uncomment to enable debugging:
  -- { import = "astrocommunity.debugging.telescope-dap-nvim" },
  -- { import = "astrocommunity.debugging.persistent-breakpoints-nvim" },
  -- { import = "astrocommunity.debugging.nvim-dap-virtual-text" },
  -- { import = "astrocommunity.debugging.nvim-dap-repl-highlights" },
  -- { import = "astrocommunity.debugging.nvim-bqf" },

  -- optional:
  -- { import = "astrocommunity.bars-and-lines.bufferline-nvim" },
  -- { import = "astrocommunity.indent.mini-indentscope" },
  --
  --
  { import = "astrocommunity.git.git-blame-nvim" },
  { import = "astrocommunity.git.diffview-nvim" },
  -- { import = "astrocommunity.git.neogit" },
  --
  -- { import = "astrocommunity.completion.cmp-cmdline" },
  --
  -- { import = "astrocommunity.color.headlines-nvim" },
  -- { import = "astrocommunity.color.mini-hipatterns" },
  -- { import = "astrocommunity.color.modes-nvim" },
  -- { import = "astrocommunity.color.tint-nvim" },
  -- { import = "astrocommunity.color.transparent-nvim" },
  --
  -- { import = "astrocommunity.search.nvim-hlslens" },
  --
  -- { import = "astrocommunity.editing-support.treesj" },
  -- { import = "astrocommunity.editing-support.refactoring-nvim" },
  -- { import = "astrocommunity.editing-support.rainbow-delimiters-nvim" },
  -- { import = "astrocommunity.editing-support.ultimate-autopair-nvim" },
  -- { import = "astrocommunity.editing-support.nvim-treesitter-endwise" },
  --
  -- -- these two plugins belong together:
  -- { import = "astrocommunity.editing-support.yanky-nvim" },
  -- { import = "astrocommunity.register.nvim-neoclip-lua" },
  --
  -- { import = "astrocommunity.programming-language-support.csv-vim" },
  -- { import = "astrocommunity.programming-language-support.rest-nvim" },
  --
  -- { import = "astrocommunity.split-and-window.windows-nvim" },
  --
  -- { import = "astrocommunity.syntax.hlargs-nvim" },
  -- { import = "astrocommunity.syntax.vim-easy-align" },
  -- { import = "astrocommunity.syntax.vim-sandwich" },
  --
  -- { import = "astrocommunity.terminal-integration.flatten-nvim" },
  -- { import = "astrocommunity.terminal-integration.vim-tmux-yank" },
  -- { import = "astrocommunity.terminal-integration.vim-tpipeline" },
  --
  -- { import = "astrocommunity.motion.marks-nvim" },
  -- { import = "astrocommunity.motion.tabout-nvim" },
  -- { import = "astrocommunity.motion.mini-ai" },
  -- { import = "astrocommunity.motion.mini-bracketed" },
  -- { import = "astrocommunity.motion.mini-move" },
  -- { import = "astrocommunity.motion.mini-surround" },
  --
  -- -- dont map H and L to not conflict with tmux left/right window switching:
  -- { 
  --   "echasnovski/mini.move",
  --   keys = {
  --     { "<C-k>", mode = { "n", "v" } },
  --     { "<C-j>", mode = { "n", "v" } },
  --   },
  --   opts = {
  --     mappings = {
  --       down = "<C-j>",
  --       up = "<C-k>",
  --       line_down = "<C-j>",
  --       line_up = "<C-k>",
  --     },
  --   },
  -- },
  --
}
