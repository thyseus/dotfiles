InputDevicesConfig = {
	--Thrustmaster TX - T100 - T300RS - T150 - T500	- TS-PC-RACER - TS-XW-RACER - T-GT
	{
		GUID = { 3060335695, 3059352655, 3060663375, 3061253199, 3059614799, 3062432847, 3063022671, 3062105167 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER0,
				DIAxis = DirectInputState.rglSlider0,
				Filter = FilterType.MaxDefaultStick,
			},

			--Duplicated Axis to be merged into InputBindings.lua

			--Move Left/Right
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRX,
				DIAxis = DirectInputState.AxislRx,
				Filter = FilterType.Stick,
			},

			--Move Up/Down
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRY,
				DIAxis = DirectInputState.AxislRy,
				Filter = FilterType.Stick,
			},

			--Rotate Left/Right
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LZ,
				DIAxis = DirectInputState.AxislZ,
				Filter = FilterType.Stick,
			},

			--Rotate Up/Down
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER1,
				DIAxis = DirectInputState.rglSlider1,
				Filter = FilterType.Stick,
			},
		},
	},

	--Thrustmaster TMX
	{
		GUID = { 3061777487 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.InvertStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.InvertStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER0,
				DIAxis = DirectInputState.rglSlider0,
				Filter = FilterType.InvertStick,
			},
		},
	},

	--Thrustmaster FERRARI F1, reversed mode
	{
		GUID = { 3060728911, 3059876943, 3060466767 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER0,
				DIAxis = DirectInputState.rglSlider0,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},

	--Logitech G27 - Logitech G25 - DF
	{
		GUID = { 3264939117, 3264808045, 3264480365 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER1,
				DIAxis = DirectInputState.rglSlider1,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},

	--DFGT - DFPRO
	{
		GUID = { 3264873581, 3264742509 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},

	--Logitech G920 - G29
	{
		GUID = { 3261203565, 3259958381 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LZ,
				DIAxis = DirectInputState.AxislZ,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRX,
				DIAxis = DirectInputState.AxislRx,
				Filter = FilterType.MaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},

	--Logitech MOMO
	{
		GUID = { 3389195373 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},

	--Simxperience Accuforce Pro v2
	{
		GUID = { 2152472521 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},
		},
	},

	--Fanatec CS v1, CS v2, CS v2.5, CSL Elite, CSL Elite PS4, CSR, Porsche 911
	{
		GUID = { 59641527, 69303, 265911, 235081399, 331447, 1117879, 26676919 },
		Axis = {
			--Steering
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER0,
				DIAxis = DirectInputState.rglSlider0,
				Filter = FilterType.MaxDefaultStick,
			},

			--Handbrake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_RGLSLIDER1,
				DIAxis = DirectInputState.rglSlider1,
				Filter = FilterType.MaxDefaultStick,
			},

			--Duplicated Axis to be merged into InputBindings.lua

			--Move Left/Right
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRX,
				DIAxis = DirectInputState.AxislRx,
				Filter = FilterType.InvertStick,
			},

			--Move Up/Down
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRY,
				DIAxis = DirectInputState.AxislRy,
				Filter = FilterType.Stick,
			},
		},
	},

	--Playstation Dualshock 3 - Playstation Sixaxis - Playstation Dualshock 4 - Playstation Dualshock 4 Neo
	{
		GUID = { 40371532, 96732492, 164365644 },
		Axis = {
			--Steering / Move X-Axis
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Move Y-Axis
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.InvertStick,
			},

			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRY,
				DIAxis = DirectInputState.AxislRy,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRX,
				DIAxis = DirectInputState.AxislRx,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Duplicated Axis to be merged into InputBindings.lua

			--Move Left/Right
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.Stick,
			},

			--Move Up/Down
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.Stick,
			},

			--Rotate Left/Right
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LZ,
				DIAxis = DirectInputState.AxislZ,
				Filter = FilterType.Stick,
			},

			--Rotate Up/Down
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.Stick,
			},
		},
	},

	--Fanatec Pedals USB V2
	{
		GUID = { 406392503 },
		Axis = {
			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRX,
				DIAxis = DirectInputState.AxislRx,
				Filter = FilterType.MaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRY,
				DIAxis = DirectInputState.AxislRy,
				Filter = FilterType.MaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRZ,
				DIAxis = DirectInputState.AxislRz,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},

	--Fanatec Pedals USB V3 - Fanatec Pedals CSL Elite
	{
		GUID = { 406523575, 1644433079 },
		Axis = {
			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LZ,
				DIAxis = DirectInputState.AxislZ,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Handbrake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LRX,
				DIAxis = DirectInputState.AxislRx,
				Filter = FilterType.Stick,
			},
		},
	},

	--Fanatec Handbrake
	{
		GUID = { 445845175 },
		Axis = {
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.InvertMaxDefaultStick,
			},
		},
	},

	--Heusinkveld Sim Pedals
	{
		GUID = { 2332168388, 274800082, 554048978 },
		Axis = {
			--Acceleration
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LZ,
				DIAxis = DirectInputState.AxislZ,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Brake
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.InvertMaxDefaultStick,
			},

			--Clutch
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LX,
				DIAxis = DirectInputState.AxislX,
				Filter = FilterType.InvertMaxDefaultStick,
			},
		},
	},

	--Thrustmaster TSS
	{
		GUID = { 3063219279 },
		Axis = {
			{
				ControlId = GenericDeviceControlId.I_ICI_GENERICDEVICE_AXIS_LY,
				DIAxis = DirectInputState.AxislY,
				Filter = FilterType.MaxDefaultStick,
			},
		},
	},
}
