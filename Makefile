MAKEFLAGS=-s

help:
	echo "Possible commands are:\n"
	cat ${MAKEFILE_LIST} | grep -v "{MAKEFILE_LIST} | grep -v" | grep ":.*##" | column -t -s "##"

basics: ## basics
	sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent git nala stow
	sudo apt-get update

stow: ## symlink farm manager
	# i usually clone my dotfiles to ~/dev/dotfiles. overwrite the default ../ target directory.
	stow --target=../../ .

cargo: ## rust stuff
	sudo nala install pkg-config libssl-dev
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	rustup update
	cargo install --locked yazi-fm yazi-cli # cli file manager
	cargo install work-break # https://lib.rs/crates/work-break
	cargo install bartib # https://github.com/nikolassv/bartib time tracking
	cargo install glim-tui # https://github.com/junkdog/glim gitlab pipelines
	cargo install rainfrog # https://github.com/achristmascarl/rainfrog sql management
	cargo install eureka # https://github.com/simeg/eureka idea management
	cargo install rust_analyzer
	cargo install bartib
	cargo install --locked bacon
	cargo install --locked cargo-nextest
	cargo install alacritty
	cargo install just
	cargo install television
	cargo install drill
	cargo install trunk
	cargo install mask
	cargo install diskonaut
	cargo install --version 0.1.0-alpha.5 gobang
	cargo install --git https://github.com/quantumsheep/sshs
	cargo install --git https://github.com/skallwar/suckit
	cargo install loco
	cargo install sea-orm-cli

ansible:
	sudo apt install -y ansible-lint ansible-mitogen sshpass
	python3 -m pip install --user ansible docker python-gitlab
	ansible-galaxy collection install prometheus.prometheus community.docker community.general
	ansible-galaxy role install geerlingguy.node_exporter

npm: ## npm and yarn
	curl -fsSL https://deb.nodesource.com/setup_current.x | sudo -E bash -
	sudo apt update
	sudo apt -y install nodejs
	curl --compressed -o- -L https://yarnpkg.com/install.sh | bash
	curl -fsSL https://bun.sh/install | bash
	export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$HOME/.local/bin/:$PATH"

fish:
	./install/fish.sh

tmux: ## docker
	install/tmux.sh

docker: ## docker
	install/docker.sh

rust: ## rust
	install/rust.sh

passff: ## passff
	curl -sSL https://codeberg.org/PassFF/passff-host/releases/download/latest/install_host_app.sh | bash -s -- firefox

nerdfont: ## jetbrains mono font
	mkdir -p ~/.fonts
	cd ~/.fonts
	wget -O ~/.fonts/JetBrainsMono.ttf https://github.com/ryanoasis/nerd-fonts/blob/95de53343d631eaed591b618ac03ba6515cfa515/patched-fonts/JetBrainsMono/Ligatures/Regular/complete/JetBrains%20Mono%20Nerd%20Font%20Complete%20Regular.ttf?raw=true
	fc-cache -f -v

php: ## php
	sudo add-apt-repository ppa:ondrej/php
	sudo apt update
	sudo nala install composer php8.4 php8.4-curl php8.4-mysql php8.4-pdo-mysql php8.4-mbstring \
	php8.4-bcmath php8.4-zip php8.4-xml php8.4-mailparse php8.4-intl php8.4-opcache php8.4-bz2 \
	php8.4-fpm php8.4-cli php8.4-tidy php8.4-redis php8.4-sqlite4 php8.4-oauth php8.4-decimal \
	php8.4-ssh2 php8.4-sockets php8.4-imap php8.4-pgsql php8.4-mongodb
	# sudo apt -y remove composer
	# php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	# sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
	# sudo chmod +x /usr/local/bin/composer
	composer global require laravel/installer deployer/deployer

xfce4: ## xfce4
	sudo nala install xfce4 xfce4-clipman xfce4-session xfce4-*-plugin xfce4-screenshooter xfce4-power-manager xfwm4-theme-*

stuff: ## a lot of packages that i collected over time and are mostly useful
	sudo nala install \
	pass pass-extension-otp pass-extension-tail pass-extension-tomb pass-git-helper webext-browserpass \
	cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev \
	python3 pipx pssh clusterssh newsboat ncdu byobu tmux tmuxp sshfs rdate unrar unrar-free \
	git-lfs git-doc git-email git-gui gitk git-cvs git-mediawiki git-svn tig fish ssh \
	openssh-server openssh-client entr build-essential make silversearcher-ag \
	python3-dev python3-pip python3.10-venv python3-setuptools \
	python3 python3-distutils python3-gi python3-gi-cairo python3-pyudev python3-xdg \
	python3-evdev gettext meson appstream-util desktop-file-utils python3-matplotlib python3-scipy \
	golang-go \
	atop btop htop iotop iftop nvtop sysdig wavemon joystick \
	apache2-utils putty-tools ncdu expect youtube-dl ffmpeg yamllint yapet \
	nmap neofetch hardinfo mc w3m links lynx sysstat isag mmdb-bin jq \
	gkrellm meld ranger nnn imagemagick imagemagick-doc delta xdelta filezilla \
	dirdiff libminizip1 uptimed plank taktuk slack cmus cifs-utils proxychains4 \
	lnav most multitail clonezilla glances smartmontools syncthing tree \
	mycli mariadb-client mariadb-backup postgresql-client-14 \
	bat ripgrep fzf exa ccrypt ncal wl-clipboard sqlite3 sox libsox-fmt-mp3 \
	hexyl ghex siege dos2unix reclass traceroute symlinks \
	libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev fswatch \
	libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb oathtool asciidoc asciidoctor lua-curl tidy \
	taskwarrior vit tasksh taskd bugwarrior keepassxc \
	libwebkit2gtk-4.1-dev file libxdo-dev libssl-dev libayatana-appindicator3-dev \
	librsvg2-dev libasound2-plugins-extra pavucontrol libasound2-dev libasound2-plugins libavcodec-dev

bonus: ## bigger, nice-to-have but not essential stuff
	sudo nala install cheese wireshark gimp gimp-data-extras flatpak remmina remmina-plugin-vnc remmina-plugin-rdp gnome-disk-utility \
	wine winetricks cool-retro-term poedit plank graphviz
	pip install litecli
	pip install upass
	install/dive.sh
	install/slim.sh

snap:
	sudo snap install onefetch
	sudo snap install mousam # weather
	sudo snap install gog-galaxy-wine
	sudo snap install insomnia
	sudo snap install bun-js

flat: 
	flatpak install io.github.seadve.Kooha # Screen recording utility
	flatpak install flathub io.github.nokse22.asciidraw

lazy: ## be lazy !
	curl -Lo /tmp/lazydocker.tar.gz https://github.com/jesseduffield/lazydocker/releases/download/v0.23.1/lazydocker_0.23.1_Linux_x86_64.tar.gz
	curl -Lo /tmp/lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/download/v0.41.0/lazygit_0.41.0_Linux_x86_64.tar.gz"
	curl -Lo /tmp/lazynpm.tar.gz "https://github.com/jesseduffield/lazynpm/releases/download/v0.1.4/lazynpm_0.1.4_Linux_x86_64.tar.gz"
	sudo tar xf /tmp/lazygit.tar.gz -C /usr/local/bin lazygit
	sudo tar xf /tmp/lazydocker.tar.gz -C /usr/local/bin lazydocker
	sudo tar xf /tmp/lazynpm.tar.gz -C /usr/local/bin lazynpm
	lazygit --version
	lazydocker --version
	lazynpm --version
	rm -f /tmp/lazygit.tar.gz
	rm -f /tmp/lazydocker.tar.gz
	rm -f /tmp/lazynpm.tar.gz
	# Other cool stuff:
	# wget https://github.com/wtfutil/wtf/releases/download/v0.42.0/wtf_0.42.0_linux_amd64.tar.gz
	# curl -LO https://github.com/ClementTsang/bottom/releases/download/0.8.0/bottom_0.8.0_amd64.deb
	# wget https://github.com/zix99/rare/releases/download/0.3.0/rare_amd64.deb
	# wget https://github.com/nytopop/askii/releases/download/v0.6.0/askii_0.6.0_amd64.deb

neovim: npm
	rm -f nvim-linux64.tar.gz
	wget https://github.com/neovim/neovim/releases/download/v0.10.0/nvim-linux64.tar.gz
	# wget https://github.com/neovim/neovim/releases/download/nightly/nvim-linux64.tar.gz
	tar xvf nvim-linux64.tar.gz
	sudo bin/backup_file.sh /usr/bin/nvim
	sudo cp -r nvim-linux64/share/* /usr/share
	sudo cp -r nvim-linux64/lib/* /lib
	sudo cp nvim-linux64/bin/nvim /usr/bin/nvim
	rm nvim-linux64.tar.gz
	rm -rf nvim-linux64
	# phpactor sucks. disable it.
	echo '' > /home/thyseus/.local/share/nvim/mason/packages/phpactor/phpactor.phar

clean-astronvim:
	rm -rf ~/.local/share/nvim
	rm -rf  ~/.local/state/nvim
	rm -rf ~/.cache/nvim

steam:
	sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	sudo flatpak install flathub com.valvesoftware.Steam
	
# only necessary on MX Linux:
# https://mxlinux.org/wiki/system/systemd/#enablingsystemd
systemd:
	sudo nala install systemd-sysv

git-reset-creds:
	git credential-cache exit

tableplus:
	wget -qO - https://deb.tableplus.com/apt.tableplus.com.gpg.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/tableplus-archive.gpg > /dev/null
	sudo add-apt-repository "deb [arch=amd64] https://deb.tableplus.com/debian/20 tableplus main"
	sudo nala update
	sudo nala install tableplus
