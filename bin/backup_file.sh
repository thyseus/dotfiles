#!/bin/bash
# Backup a file or directory.
# *moves* instead of *copies* to overwrite source file/directory directly after this command.

date=$(date +%F)

for var in "$@"; do
	mv $var $var-$date-$RANDOM.backup 2>/dev/null
done

# exit regardless of success:
exit 0
