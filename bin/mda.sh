#!/bin/bash

while read line
do
	echo "$line" >> /tmp/mail-$(date +%s%N).eml
done < "${1:-/proc/${$}/fd/0}"
