#!/usr/bin/fish

rm -rf /home/dgrosse/.local/share/omf
rm -rf /home/dgrosse/.config/fish
rm -rf /home/dgrosse/.config/omf

# FISH recent version:
echo 'deb http://download.opensuse.org/repositories/shells:/fish/Debian_11/ /' | sudo tee /etc/apt/sources.list.d/shells:fish.list
curl -fsSL https://download.opensuse.org/repositories/shells:fish/Debian_11/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/shells_fish.gpg >/dev/null
sudo apt update
sudo apt install fish

# Fisher
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher

# https://github.com/joseluisq/gitnow
fisher install joseluisq/gitnow@2.11.0

# https://github.com/acomagu/fish-async-prompt
fisher install acomagu/fish-async-prompt

# https://github.com/Gazorby/fish-abbreviation-tips
fisher install gazorby/fish-abbreviation-tips

# https://github.com/jorgebucaran/spark.fish
fisher install jorgebucaran/spark.fish

# https://github.com/joseluisq/gitnow
fisher install joseluisq/gitnow

# https://github.com/meaningful-ooo/sponge
fisher install meaningful-ooo/sponge

# https://github.com/jorgebucaran/autopair.fish
fisher install jorgebucaran/autopair.fish

# https://github.com/jorgebucaran/hydro
fisher install jorgebucaran/hydro

# https://github.com/oh-my-fish/oh-my-fish
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
curl -sL https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install.sha256 | shasum -a 256 --check

# https://github.com/PatrickF1/fzf.fish
fisher install PatrickF1/fzf.fish

omf install neolambda

# https://stackoverflow.com/questions/2762994/define-an-alias-in-fish-shell
alias sail vendor/bin/sail
funcsave sail

alias a "vendor/bin/sail artisan"
alias artisan "vendor/bin/sail artisan"
funcsave a
funcsave artisan

alias t "vendor/bin/sail artisan tinker"
alias tinker "vendor/bin/sail artisan tinker"
funcsave t
funcsave tinker

chsh thyseus -s /usr/bin/fish

fish_add_path ~/.cargo/bin
fish_add_path ~/.local/bin/
fish_add_path ~/bin/

# https://github.com/sharkdp/bat?tab=readme-ov-file#on-ubuntu-using-apt
ln -s /usr/bin/batcat ~/bin/bat

set -Ux MOZ_ENABLE_WAYLAND 1
set -Ux BARTIB_FILE ~/Sync/time.bartib
touch ~/Sync/time.bartib
