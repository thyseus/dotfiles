curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
cargo install cargo-watch
rustup component add clippy
rustup component add rustfmt
